#include <stdio.h>
#include <stdarg.h>
#include <blep/debug.h>

void print_debug(char *format, ...)
{
    va_list args;
    va_start(args, format);
    fprintf(stderr, format, args);
    va_end(args);
}
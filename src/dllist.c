#include <blep/debug.h>
#include "dllist.h"

dllist_head_t *dllist_new(print_f print, destroy_f destroy, cmp_f cmp)
{
    if (NULL == print ||
        NULL == destroy ||
        NULL == cmp)
    {
        DEBUG_PRINT("A print and free function are needed.\n");
        return NULL;
    }

    dllist_head_t *new_head = calloc(1, sizeof(dllist_head_t));
    new_head->destroy = destroy;
    new_head->cmp = cmp;
    new_head->print = print;
    return new_head;
}

void dllist_destroy(dllist_head_t *head)
{
    if (NULL == head)
    {
        return;
    }

    dllist_node_t *cur_node = head->nodes;
    while (cur_node != NULL)
    {
        dllist_node_t *next_node = cur_node->next;
        head->destroy(cur_node->item);
        free(cur_node);

        cur_node = next_node;
    }

    free(head);
    head = NULL;
}

bool dllist_add(dllist_head_t *head, void *item)
{
    if (NULL == head || NULL == item)
    {
        DEBUG_PRINT("No list or item provided.\n");
        return false;
    }

    dllist_node_t *new_node = calloc(1, sizeof(dllist_node_t));
    if (NULL == new_node)
    {
        DEBUG_PRINT("Failed new node memory allocation.\n");
        return false;
    }

    new_node->item = item;
    new_node->index = head->total_nodes;

    dllist_node_t *cur_node = head->nodes;
    if (NULL == cur_node)
    {
        cur_node = new_node;
        new_node->next = NULL;
        new_node->prev = NULL;
    }
    else
    {
        while (cur_node->next != NULL)
        {
            cur_node = cur_node->next;
        }

        new_node->prev = cur_node;
        cur_node->next = new_node;
    }

    head->total_nodes++;
    return true;
}

size_t dllist_size(dllist_head_t *head)
{
    if (NULL == head) {return 0;}
    return head->total_nodes;
}
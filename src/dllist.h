#ifndef _DLLIST_H
#define _DLLIST_H

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

typedef void (*destroy_f)(void *);
typedef void (*print_f)(void *);
typedef int (*cmp_f)(void *, void *);

typedef struct dllist_node {
    void *item;

    struct dllist_node *next;
    struct dllist_node *prev;

    int index;
} dllist_node_t;

typedef struct dllist_head {
    size_t total_nodes;
    destroy_f destroy;
    print_f print;
    cmp_f cmp;

    dllist_node_t *nodes;    
} dllist_head_t;

#endif
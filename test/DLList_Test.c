#include <stdio.h>
#include <stdlib.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include <blep/dllist.h>

dllist_head_t *head;

void user_print(void *item)
{
    if (NULL == item)
    {
        return;
    }

    printf("%d\n", *(int *)item);
}

int user_cmp(void *a, void *b)
{
    int *num1 = a;
    int *num2 = b;
    if (*num1 == *num2) return 0;
    if (*num1 < *num2) return -1;
    if (*num1 > *num2) return 1;
}

int init_suite_llist(void)
{
    head = NULL;
    return 0;
}

int clean_suit_dllist(void)
{
    dllist_destroy(head);
    return 0;
}

void test_new_dllist(void)
{
    CU_ASSERT_PTR_NULL(dllist_new(NULL, NULL, NULL));
    CU_ASSERT_PTR_NULL(dllist_new(user_print, NULL, user_cmp))
    CU_ASSERT_PTR_NULL(dllist_new(NULL, free, user_cmp))
    CU_ASSERT_PTR_NULL(dllist_new(user_print, free, NULL))

    head = dllist_new(user_print, free, user_cmp);
    CU_ASSERT_PTR_NOT_NULL(head)
}

void test_dllist_add(void)
{
    int *nums = calloc(10, sizeof(int));
    for (int i = 0; i < 10; i++)
    {
        nums[i] = i*2;
    }

    CU_ASSERT_EQUAL(dllist_add(NULL, NULL), false);
    CU_ASSERT_TRUE(dllist_add(head, nums+1));
    CU_ASSERT_TRUE(dllist_add(head, nums+2));
}

void test_dllist_sz(void)
{
    CU_ASSERT_EQUAL(dllist_size(NULL), 0);
    CU_ASSERT_EQUAL(dllist_size(head), 2);
}

int main(void)
{
    CU_pSuite new_dllist_suite = NULL;

    if (CUE_SUCCESS != CU_initialize_registry())
    {
        return CU_get_error();
    }

    new_dllist_suite = CU_add_suite("Creating New Doubly LList", init_suite_llist, clean_suit_dllist);
    if (NULL == new_dllist_suite)
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(new_dllist_suite, "Test of create new linked list.", test_new_dllist) ||
        NULL == CU_add_test(new_dllist_suite, "Test of adding to linked list.", test_dllist_add) ||
        NULL == CU_add_test(new_dllist_suite, "Test of checking linked list size.", test_dllist_sz))
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
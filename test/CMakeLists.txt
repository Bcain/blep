function(add_test target source)
    add_executable(${target} ${source}.c)
    target_link_libraries(${target} blep cunit)
endfunction()

add_test(dll DLList_Test)
#ifndef DLLIST_H
#define DLLIST_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct dllist_head dllist_head_t;

typedef void (*destroy_f)(void *);
typedef void (*print_f)(void *);
typedef int (*cmp_f)(void *, void *);

/**
 * @brief Initialize doubly linked list.
 * 
 * @param print User defined print function.
 * @param destroy User defined destroy function.
 * @param cmp user defined compare function.
 * @return dllist_head_t* Returns pointer to new doubly linked list head or NULL on failure.
 */
dllist_head_t *dllist_new(print_f print, destroy_f destroy, cmp_f cmp);

/**
 * @brief Completely frees doubly linked list and head.
 * 
 * @param head The supplied head to a doubly linked list.
 */
void dllist_destroy(dllist_head_t *head);

/**
 * @brief Addeds an item to the end of the doubly linked list.
 * 
 * @param head The supplied head to a doubly linked list.
 * @param item The user supplied item.
 * @return true If the item was added successfully.
 * @return false If the item failed to be added.
 */
bool dllist_add(dllist_head_t *head, void *item);

/**
 * @brief Removes user supplied item from the doubly linked list.
 * 
 * @param head The supplied head to a doubly linked list.
 * @param item User supplied item.
 * @return true If the item was removed successfully.
 * @return false If the item failed to be removed.
 */
bool dllist_rm(dllist_head_t *head, void *item);

/**
 * @brief 
 * 
 * @param head The supplied head to a doubly linked list.
 * @param item User supplied item.
 * @param index The location where item is to be inserted.
 * @return true If the item was added successfully.
 * @return false If the item failed to be added.
 */
bool dllist_insert(dllist_head_t *head, void *item, size_t index);

/**
 * @brief Will return the size of the dllist provided.
 * 
 * @param head The main container for the dllist.
 * @return size_t The total number of nodes in the dllist.
 * @return NULL on failure.
 */
size_t dllist_size(dllist_head_t *head);
#endif
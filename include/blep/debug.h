#ifndef DEBUG_H
#define DEBUG_H

#ifdef BLEP_DEBUG
    #define DEBUG_PRINT(msg, ...) do {fprintf(stderr, "\n%s:%s:%d: %s", __FILE__, __func__, __LINE__, msg);} while(0)
#else
    #define DEBUG_PRINT(msg, ...) 
#endif

#endif